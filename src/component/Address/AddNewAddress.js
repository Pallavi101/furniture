import React, { useEffect, useState } from 'react';
//import { StyleSheet, View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';
import { StyleSheet, View, Text, ScrollView, TouchableOpacity, TextInput, SafeAreaView, Button, Alert, Dimensions } from 'react-native';
import { Icon } from 'react-native-elements';
import CommonHeader from '@src/component/header';
import { Card } from 'react-native-shadow-cards';
import { withNavigation } from 'react-navigation';
//import { RadioButton } from 'react-native-paper';




const Box = ({ txt }) => {
  return (
    <View style={styles.box}>
      <TouchableOpacity>
        <Text style={{ alignSelf: 'center', marginTop: 5 }}>{txt}</Text>
      </TouchableOpacity>
    </View>
  )
}

// onSubmit = () =>{
//   Alert.alert('Added')
// }




const AddNewAddress = ({ navigation },) => {
  const [firstname, setFirstname] = useState("")
  const [lastname, setLastname] = useState("")
  const [address, setAddress] = useState("")
  const [street, setStreet] = useState("")
  const [city, setCity] = useState("")
  const [pincode, setPincode] = useState("")
  const [modal, setModal] = useState(false)




  //    onFetchLoginRecords =()=> {
  //     var data = {
  //       firstname:firstname,
  //       lastname:lastname,
  //       address:address,
  //       street:street,
  //       city:city,
  //       pincode:pincode


  //     };
  //     try {
  //      let response = fetch(
  //       "http://yourdomain.com",
  //       {
  //         method: "POST",
  //         headers: {
  //          "Accept": "application/json",
  //          "Content-Type": "application/json"
  //         },
  //        body: JSON.stringify(data)
  //      }
  //     );
  //      if (response.status >= 200 && response.status < 300) {
  //         alert("authenticated successfully!!!");
  //      }
  //    } catch (errors) {

  //      alert(errors);
  //     } 
  // }

  return (
    <SafeAreaView>
      <CommonHeader onPress={() => navigation.toggleDrawer()}
        onSearchIconPress={() => navigation.navigate('Routes', { screen: 'HomeSearch' })}
        onCartIconPress={() => navigation.navigate('Routes', { screen: 'HomeSearch' })}
        onProfileIconPress={() => navigation.navigate('Routes', { screen: 'HomeProfile' })}
      />
      <Card elevation={1} style={styles.card}>
        <Text style={styles.InsideText}>   Address Details </Text>
        <View style={{ marginLeft: 10 }}>
          <TextInput
            placeholder="Firstname "
            value={firstname}
            underlineColorAndroid="transparent"
            onChangeText={text => setFirstname(text)}
          />
          <View style={styles.lineStyle} />
        </View>

        <View style={{ marginLeft: 10 }}>
          <TextInput
            placeholder="Lastname "
            value={lastname}
            underlineColorAndroid="transparent"
            onChangeText={text => setLastname(text)}
          />
          <View style={styles.lineStyle} />
        </View>

        <View style={{ marginLeft: 10 }}>
          <TextInput
            placeholder="address "
            value={address}
            underlineColorAndroid="transparent"
            onChangeText={text => setAddress(text)}
          />
          <View style={styles.lineStyle} />
        </View>

        <View style={{ marginLeft: 10 }}>
          <TextInput
            placeholder="street "
            value={street}
            underlineColorAndroid="transparent"
            onChangeText={text => setStreet(text)}
          />
          <View style={styles.lineStyle} />
        </View>

        <View style={{ marginLeft: 10 }}>
          <TextInput
            placeholder="city "
            value={city}
            underlineColorAndroid="transparent"
            onChangeText={text => setCity(text)}
          />
          <View style={styles.lineStyle} />
        </View>

        <View style={{ marginLeft: 10 }}>
          <TextInput
            placeholder="pincode "
            value={pincode}
            underlineColorAndroid="transparent"
            onChangeText={text => setPincode(text)}
          />
          <View style={styles.lineStyle} />
        </View>


        <Text style={styles.InsideText}> Save address as </Text>
        <View style={{ flexDirection: 'row', }}>
          <Box txt='Home' />
          <Box txt='Office' />
          <Box txt='Others' />
        </View>

        <View>
          <View style={styles.lineStyle} />
        </View>

        <View style={{ margin: 20 }}>
          <Button
            color="#f18721"
            title="Add Address "
            onPress=
            {() => Alert.alert('Added')}
          // {() => { this.onSubmit(); this.props.navigation.navigate('NextScreen') }}
          />
        </View>


        <View style={{ margin: 20 }}>
          <Button
            color="#f18721"
            title="Proceed To Checkout "
            onPress={() => Alert.alert('Checkout')}

          // onPress={()=>navigation.navigate('AddNewAddress')}

          />
        </View>


      </Card>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({

  card: {
    height: hp('85%'), // 30% of height device screen
    width: wp('94%'), // 100% of width device screen
    resizeMode: 'stretch',
    margin: hp(1),
  },

  lineStyle: {
    borderWidth: 0.5,
    borderColor: '#e3e3e3',
    margin: 10,
    // flexDirection: 'column',
  },

  InsideText: {
    fontFamily: "open-serif",
    marginTop: 10,
    paddingBottom: 10,
    fontSize: 15,
    color: '#333333',
    //textAlign: 'center',
    //   marginTop: 10,
    // marginLeft: 10,
    fontWeight: 'bold',
    flexDirection: 'column',
    marginStart: 5

  },

  box: {
    height: hp('4%'),
    width: wp('18%'),
    borderWidth: 1,
    marginLeft: 5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderColor: '#e3e3e3',
    backgroundColor: '#f5f4f2',
  },


})
export default AddNewAddress