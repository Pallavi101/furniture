import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Image, FlatList, Text, TouchableOpacity, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';
import GetApiCall from '../../../GetApi';

const Banner = () => {
    const [resState, setRes] = useState(null)
    // const [isloading , setLoading] = useState(true)
    
  useEffect(() => {
  
    FetchBanner();

  }, []);

    async function FetchBanner() {
        var arr = [];
        const bannerData = await GetApiCall.getRequest("Main-Top-Banner");
        // setRes(bannerData)
        //  console.warn(bannerData);
        for (var i = 0; i <= 6; i++) {
            arr.push(bannerData.MainTopBanner.Products[i]);

            setRes(arr);
        }
        console.log(resState);
        console.warn(resState.MainTopBanner.Products)
        console.warn('fetch banner running first')
    }

    const RenderBanner = ({ item, index }) => {
        FetchBanner()
        return (<Image
            style={styles.bannerRectangle}
            source={{ uri:`${ resState.uri}` }}
            keyExtractor={(item, index) => `flat_${index}`}

        //     {{uri:`${res.MainTopBanner.Products[0].uri}`}}
                 

        />)
    }
    return (

        <FlatList
            data={resState}
            renderItem={RenderBanner}
            //keyExtractor={(resState) => resState.id}

        />

        //     <FlatList
        //     horizontal
        //     scrollEnabled={true}
        //     showsHorizontalScrollIndicator={false}
        //     data={Blog}
        //     renderItem={renderItemBlog}
        //     keyExtractor={(item, index) => `flat_${index}`}
        //   />

    )

}


{/* <FastImage
            style={{
              width: Platform.OS == 'ios' ? 350 : 400,
              height: 250,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              alignSelf: 'center',
            }}
            source={{
              uri: item.fld_coverimage,
              //  
              priority: FastImage.priority.high,
            }}
            resizeMode={FastImage.resizeMode.stretch}
          /> */}

const styles = StyleSheet.create({
    bannerRectangle: {

        width: wp('70%'),
        height: hp('22%'),
        borderWidth: 2,
        borderRadius: 10,
        margin: 4,
        marginLeft: 10,


    },
    itemRow: {
        borderBottomColor: '#ccc',
        marginBottom: 10,
        //  borderBottomWidth:1,
    },
    itemText: {
        fontSize: 16,
        padding: 5,
    },
    itemImage:
    {
        width: wp('30%'),
        height: hp('8%'),
        resizeMode: 'cover'
    },

})
export default Banner