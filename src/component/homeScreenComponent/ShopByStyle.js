import React from 'react';
import { StyleSheet,View ,Text, Image, ScrollView ,TouchableOpacity,FlatList} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';
import CardView from 'react-native-cardview'
import { useNavigation } from '@react-navigation/native';

const ShopByRoom = [
  {
    "id": "1",
    "roomType": "3 Seater Sofa",
    "src": require('@assets/room1.jpg'),
  },

  {
    "id": "2",
    "roomType": "King Sized Bed",
    "src": require('@assets/room2.jpg'),
  },

  {
    "id": "3",
    "roomType": "Dining Table",
    "src": require('@assets/room3.jpg'),

  },

  {
    "id": "4",
    "roomType": "3 Seater Sofa",
    "src": require('@assets/room4.jpg'),

  },

  {
    "id": "5",
    "roomType": "King Sized Bed",
    "src": require('@assets/ROOM5.jpg'),

  },

  {
    "id": "6",
    "roomType": "Dining Table",
    "src": require('@assets/room6.jpg'),

  },
]


const ShopByStyle = ( props) => {

  return (
  
    <FlatList
      data={ShopByRoom}
      //  data={data}
      showsHorizontalScrollIndicator={false}
      horizontal={true}
      keyExtractor={(item) => item.id}
      renderItem={({ item, index }) =>
      (<Card
        img={item.src}
        textName={item.roomType}
        key={index}
      />)}
    />

  );
}
const Card = ({roomType, textName, key,img}) => {
  const navigation = useNavigation();
  return(
  <View style={styles.MainContainer}>
    <TouchableOpacity>
       {/* <TouchableOpacity title="Next" onPress={() => { navigation.navigate('ShopByRoomType')}}> */}
    <CardView
     cardElevation={5}
      cardMaxElevation={10}
      cornerRadius={5}
      style={styles.cardViewStyle}>
      <Image style={styles.shopByRoomImage}
       //source={Request}
       source={img}
       key={key} />
      <Text style={styles.cardView_InsideText}>{ textName}</Text>
    </CardView>
    </TouchableOpacity>
  </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: Constants.statusBarHeight,
  },

  cardView_InsideText:
  {
    fontFamily: "sans-serif-light",
    fontSize: 14,
    color: '#333333',
    textAlign: 'center',
    marginTop: 5


  },
  cardViewStyle: {

    width: wp('50%'),
    height: hp('22%'),
    marginLeft: 11,
    marginTop: 5,
    marginBottom: 5

  },


  shopByRoomImage:
  {
    width: wp('50%'),
    height: hp('18%'),
  }

});
export default ShopByStyle;