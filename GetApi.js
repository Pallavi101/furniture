//var urlString = 'https://api.beatmysugar.com/BackofficeApi/';
var urlString = 'http://demo.globaltrendz.online/furnmart/furn-mart-api/FunMart/';

const GetApiCall = {
  getRequest(url) {
    return fetch(urlString + url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => 
        // console.log(response)
         response.json()
      ).then((res)=>{
        return res
      })
      .catch((error) => {
        console.log('request failed', error);
        return error;
      });
  },
};

export default GetApiCall;
